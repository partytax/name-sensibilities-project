#import the wonderful pandas library
import pandas as pd
pd.set_option('display.max_rows', 1000)
#import numpy
import numpy as np

#import flask web framework
from flask import Flask, render_template
#instantiate a Flask app
app = Flask(__name__)

from data import Articles
Articles = Articles()


@app.route("/")
def index():
    return render_template('home.html')

@app.route("/about")
def about():
        return render_template('about.html')

@app.route("/articles")
def articles():
        return render_template('articles.html', articles = Articles)

@app.route("/article/<string:id>")
def article(id):
        return render_template('article.html', id=id)




if __name__ == '__main__':
    app.run()

#input parameters to request the right information
start_year = int(input("Start Year: "))
#quick shortcut that inputs default start and end years if zero is typed
if start_year == 0:
    start_year = 1880
    end_year = 2018
else:
    end_year = 1 + int(input("End Year: "))
gender = input("Gender:").upper()
names_per_year = int(input('Names per year: '))


#Create DataFrame to hold data from census CSV files
combined_data = pd.DataFrame()
#iterate through csv files to produce dataframe of top 100 names from each year
for year in range(start_year, end_year):
    year_names = pd.read_csv(f'names/yob{year}.txt', header=None, names=["name", 'gender', 'occurrence'])
    gendered_year_names = year_names[year_names['gender'] == gender].copy(deep=True)
    gendered_year_names.sort_values(by=['occurrence'], ascending=False, inplace=True)
    gendered_year_names_100 = gendered_year_names.head(names_per_year)
    combined_data = pd.concat([combined_data, gendered_year_names_100])


#Create DataFrame of unique names from combined name data
unique_names = pd.DataFrame(combined_data['name'].unique())
#Set column name to 'name'
unique_names.columns = ['name']
#Create 'preference_score' column and fill it with zeros as a neutral starting score
unique_names['preference_score'] = pd.Series(np.zeros(len(unique_names)))
#Print the resulting table and its length
print(unique_names)
print(len(unique_names))


#Enter name ranking cycle
while True:

    #Generate two name index integers
    while True:
        name_1_index = np.random.randint(0,len(unique_names))
        name_2_index = np.random.randint(0,len(unique_names))
        if name_1_index != name_2_index:
            break
    #Get names corresponding to those indices
    name_1 = unique_names.iloc[name_1_index]
    name_2 = unique_names.iloc[name_2_index]

    #Present names for the user to choose
    print("Which name do you prefer?\n")
    print(f'{name_1[0]}---1---2---3---4---5---{name_2[0]}')

    #Take user's ranking from 1 to 5
    preference = input()

    #Assign points based on name preference
    if preference == '1':
        #Reward preferred name with +2 points and penalize other name with -2 points
        unique_names.iloc[name_1_index,1] += 2
        unique_names.iloc[name_2_index,1] -= 2
    elif preference == '2':
        #Reward preferred name with +1 point and penalize other name with -1 point
        unique_names.iloc[name_1_index,1] += 1
        unique_names.iloc[name_2_index,1] -= 1
    elif preference == '3':
        #Do nothing, because neither name is preferred
        pass
    elif preference == '4':
        #Reward preferred name with +1 point and penalize other name with -1 point
        unique_names.iloc[name_1_index,1] -= 1
        unique_names.iloc[name_2_index,1] += 1
    elif preference == '5':
        #Reward preferred name with +2 points and penalize other name with -2 points
        unique_names.iloc[name_1_index,1] -= 2
        unique_names.iloc[name_2_index,1] += 2
    elif preference == 'v':
        #Print the current state of the ranking table
        print(unique_names.sort_values(by='preference_score'))
        print()
    elif preference == 'x':
        break
    else:
        print('***INVALID INPUT***')
